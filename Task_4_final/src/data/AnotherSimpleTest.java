package data;

public class AnotherSimpleTest {

	public static void main(String[] args) {
        AnotherSimple obj1 = new AnotherSimple(10.0, 20.0);
        AnotherSimple obj2 = new AnotherSimple(5.0, 10.0);

        TestingFramework.checkEquals(obj1.getA(), 10.0);
        TestingFramework.checkEquals(obj1.getB(), 20.0);
        TestingFramework.checkNotEquals(obj1.getA(), obj2.getA());
        TestingFramework.checkNotEquals(obj1.getB(), obj2.getB());

        TestingFramework.report();
        }
}

	 
