package data;

import java.util.ArrayList;
import java.util.List;

public class TestingFramework {
    private static List<String> checks;
    private static int checksMade = 0;
    private static int passedChecks = 0;
    private static int failedChecks = 0;

    private static void addToReport(String txt) {
        if (checks == null) {
            checks = new ArrayList<>();
        }
        checks.add(String.format("%04d: %s", checksMade++, txt));
    }

    public static <T> void checkEquals(T value1, T value2) {
        if (value1 == null && value2 == null) {
            addToReport("  null == null");
            passedChecks++;
        } else if (value1 != null && value1.equals(value2)) {
            addToReport(String.format("  %s == %s", value1, value2));
            passedChecks++;
        } else {
            addToReport(String.format("* %s == %s", value1, value2));
            failedChecks++;
        }
    }

    public static <T> void checkNotEquals(T value1, T value2) {
        if (value1 == null && value2 != null || value1 != null && !value1.equals(value2)) {
            addToReport(String.format("  %s != %s", value1, value2));
            passedChecks++;
        } else {
            addToReport(String.format("* %s != %s", value1, value2));
            failedChecks++;
        }
    }

    public static void report() {
        System.out.printf("%d checks passed\n", passedChecks);
        System.out.printf("%d checks failed\n", failedChecks);
        System.out.println();

        for (String check : checks) {
            System.out.println(check);
        }
    }
}
